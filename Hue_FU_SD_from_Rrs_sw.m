function [alfa,FU,z_SD]=Hue_FU_SD_from_Rrs_sw(Rrs)
% [alfa,FU,z_SD]=Hue_FU_SD_from_Rrs_sw(Rrs)
% Matlab implementation of the algorithms that compute the Hue angle, the
% Forel-Ule index and the Secchi disk depth from a remote-sensing
% reflectance spectrum, given at the SeawiFS bands, a.k.a. [412 443 490 510 555 670] nm
% This band setting is the same as of the ESA-OC-CCI Rrs, thus being
% applicable to its very long time series for climate studies. Also, some
% commercial in-situ radiometers use this bands and the algorithm can be
% used for output data from them too.
%
% Relevant citations for algorithms:
%   · Hue angle:  (vd Woerd and Wernand,  2015); (Pitarch et al. 2019)
%   · Forel-Ule index:  (Novoa et al., 2013); (vd Woerd and Wernand,  2015); (Pitarch et al. 2019)
%   · Secchi disk depth:  (Lee et al. 2002); (Lee et al. 2013); (Lee et al.,  2015)
%
% References:
%
% Lee, Z., Shang, S., Hu, C., Du, K., Weidemann, A., Hou, W., ... & Lin, G. (2015). Secchi disk depth:
% A new theory and mechanistic model for underwater visibility. Remote sensing of environment, 169, 139-149.
%
% Lee, Z., Hu, C., Shang, S., Du, K., Lewis, M., Arnone, R., & Brewin, R. (2013). Penetration of
% UV‐visible solar radiation in the global oceans: Insights from ocean color remote sensing.
% Journal of Geophysical Research: Oceans, 118(9), 4241-4255.
%
% Mason, J. D., Cone, M. T., & Fry, E. S. (2016). Ultraviolet (250–550 nm)
% absorption spectrum of pure water. Applied optics, 55(25), 7163-7172.
%
% Novoa, S., Wernand, M. R., & Van der Woerd, H. J. (2013). The Forel-Ule scale revisited spectrally:
% preparation protocol, transmission measurements and chromaticity. Journal of the European
% Optical Society-Rapid publications, 8.
%
% Pitarch, J., van der Woerd, H. J., Brewin, R. J., & Zielinski, O. (2019). Optical properties
% of Forel-Ule water types deduced from 15 years of global satellite ocean color observations.
% Remote Sensing of Environment, 231, 111249.
%
% Pope, R. M., & Fry, E. S. (1997). Absorption spectrum (380–700 nm) of pure water.
% II. Integrating cavity measurements. Applied optics, 36(33), 8710-8723.
%
% Woerd, H. J., & Wernand, M. R. (2015). True colour classification of natural waters with
% medium-spectral resolution satellites: SeaWiFS, MODIS, MERIS and OLCI. Sensors, 15(10), 25663-25680.
%
% Zhang, X., & Hu, L. (2009). Estimating scattering of pure water from density fluctuation
% of the refractive index. Optics Express, 17(3), 1671-1678.
%
% Input data:
%	· Rrs:      	Remote-sensing reflectance, at the SeaWiFS bands (dimensions 6x1, Units: sr^(-1))
%
% Output data:
%	· alfa:				Hue angle (Real value. Units: º)
%	· FU:   			Forel-Ule index (Real value. Units: none)
%	· z_SD:   			secchi disk depth (Real value. Units: m)


%-----------------constants-------------------
FU_can=[
    0.181336817661941   0.119816846312062
    0.191 0.167
    0.199 0.200
    0.210 0.240
    0.227 0.288
    0.246 0.335
    0.266 0.376
    0.291 0.412
    0.315 0.440
    0.337 0.462
    0.363 0.476
    0.386 0.487
    0.402 0.481
    0.416 0.474
    0.431 0.466
    0.446 0.458
    0.461 0.449
    0.475 0.441
    0.489 0.433
    0.503 0.425
    0.516 0.416
    0.528 0.408
    ];%these are the 21 canonical Forel-Ule color coordinates on the CIE 1931 xy plane, as determined by Novoa et al. (2013),
% and a lower-end addition to add resolution for oligotrophic waters, as explained by Pitarch et al. (2019)
coef_hue=[2.957 10.861 3.744 3.455 52.304 32.825;...
    0.112 1.711 5.672 21.929 59.454 17.810;...
    14.354 58.356 28.227 3.967 0.682 0.018];% 3x6 matrix that transforms Rrs into the colorimetric (X,Y,Z) triad,
% as determined by vd Woerd and Wernand (2015)

alfa_FU_can=atan2(FU_can(:,2)-1/3,FU_can(:,1)-1/3)*180/pi; %transforming the canonical xy coordinates to polar angle,
% with the white point (1/3,1/3) as origin: hue angle
alfa_FU_can(alfa_FU_can<0)=alfa_FU_can(alfa_FU_can<0)+360; %bringing negative results to the first positive loop

m=0.3017;s=0.07398; %mean and standard deviation for input data normalization

px=[  -0.000351031758365
    0.000915670754262
    0.004088603799501
    0.000343256705473
    -0.021460866467693
    -0.020031850640818
    0.029652894774097]; % x regression polynomial coefficients

py=[  -0.000325281739931
    -0.000245820783391
    -0.001066956589880
    0.004413544470727
    0.012187698559781
    -0.015604223652105
    -0.007786276733641];% y regression polynomial coefficients

%----------Diffuse attenuation model coefficients----------------------------------------------
gamma=0.265;
m1=4.259;
m2=0.52;
m3=10.8;
%----------End of Diffuse attenuation model coefficients---------------------------------------
l=[412 443 490 510 555 670];% Wavelengths
%----------Pure water absorption and backscattering coefficients----------------------------------------------
aw=[0.00271        0.006       0.0146        0.033       0.0596        0.439];% Mason et al. (2018), Pope and Fry (1997)
bbw=[0.0029957    0.0021962    0.0014318    0.0012096    0.0008482   0.00038706];% Zhang et al. (2009) for marine water
%----------End of pure water absorption and backscattering coefficients---------------------------------------

%----------Raman correction coefficients----------------------------------------------
al=[0.003 0.004 0.011 0.013 0.017 0.018];
be1=[0.014 0.015 0.010 0.010 0.010 0.010];
be2=[-0.022 -0.023 -0.051 -0.060 -0.080 -0.081];
%----------End of Raman correction coefficients---------------------------------------

g0=0.089;g1=0.1245;%QAA Rrs to u coefficients (Lee et al., (2002))

%----------end of constants----------------------------------------------

%--------Hue angle and Forel-Ule calculation---------------------

Rrs=Rrs(:)'; % it has to be a row vector
XYZ=Rrs*coef_hue';%(X,Y,Z) calculation
x_d=XYZ(1)/sum(XYZ); % biased x coordinate calculation
y_d=XYZ(2)/sum(XYZ); % biased y coordinate calculation
dx=polyval(px,(x_d-m)/s); % x bias estimation based on the biased x
x_ok=x_d-dx; % x bias compensation
dy=polyval(py,(x_d-m)/s); % y bias estimation based on the biased x
y_ok=y_d-dy; % y bias compensation

alfa=atan2(y_ok-1/3,x_ok-1/3)*180/pi; % transformation from (x,y) to hue angle
if alfa<0,alfa=alfa+360;end % bringing negative angles to the first loop

% In particular, some negative spectra at blue wavelengths have been observed.
% This condition ensures a minimum filtering

[~,kk]=min(abs(alfa-alfa_FU_can));% FU calculation from the hue angle: search for
% the nearest canonical hue angle and store its index
FU=kk-1;
%--------End of Hue angle and Forel-Ule calculation-------------

%------QAA application------------------------------------------
%------Rrs(670) correction if off reasonable limits-----------------------------
Rrs670_max=20*Rrs(5)^1.5;
Rrs670_min=0.9*Rrs(5)^1.7;
if Rrs(6)<Rrs670_min & Rrs(6)>Rrs670_max
    Rrs(6)=1.27*Rrs(5)^1.47+0.00018*(Rrs(3)/Rrs(5))^3.19;
end
%------End of Rrs(670) correction if off reasonable limits----------------------

%------Raman scattering correction-----------------------------
RF=al*Rrs(2)/Rrs(5)+be1.*Rrs(5).^be2;% Raman correction coefficients
Rrs=Rrs./(1+RF);% Raman correction
rrs=Rrs./(0.52+1.7*Rrs);% rrs from Rrs
%------End of Raman scattering correction----------------------

%------Estimation of absorpition at a reference band-----------------------------
if Rrs(6)<1.5e-3% For not so turbid water
    l0=555;
    i_l0=5;
    chi=log10((rrs(2)+rrs(3))/(rrs(5)+5*rrs(6)/rrs(3)*rrs(6)));
    al0=aw(i_l0)+10^(-1.146-1.366*chi-0.469*chi^2);
else% For turbid water
    l0=670;
    i_l0=6;
    al0=aw(i_l0)+0.39*(Rrs(6)/(Rrs(2)+Rrs(3)))^1.14;
end
%------End of estimation of absorption at a reference band----------------------

u=(-g0+sqrt(g0^2+4*g1*rrs))/(2*g1);
bbpl0=u(i_l0)*al0/(1-u(i_l0))-bbw(i_l0);
eta=2*(1-1.2*exp(-0.9*rrs(2)/rrs(5)));
bbp=bbpl0*(l0./l).^eta;
bb=bbp+bbw;
a=(1-u).*bb./u;
a(a<aw)=aw(a<aw);% If absorption is lower than that of water, it is set to water absorption
bb=u.*a./(1-u);% Recalculation of bb to ensure closure
%------End of QAA application------------------------------------------

%-------------Kd model (Lee et al., (2013))----------------------------
eta=bbw./bb;
Kd=a+(1-gamma*eta)*m1.*(1-m2*exp(-m3*a)).*bb; % Spectral diffuse attenuation coefficient
%-------------End of Kd model (Lee et al., (2013))---------------------
%-------------z_SD model (Lee et al., (2015))----------------------------
[Kdmin,i_Kdmin]=min(Kd);
z_SD=1/(2.5*Kdmin)*log(abs(0.14-Rrs(i_Kdmin))/0.013); % Lee et al. (2015)
%-------------End of z_SD model (Lee et al., (2015))---------------------