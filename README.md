**README** Hue angle, Forel-Ule and Secchi disk depth estimations from Remote-sensing reflectance measurements at the SeaWiFS bands

5 January 2021

Computer implementations of the algorithms that compute the Hue angle, the
Forel-Ule index and the Secchi disk depth from a remote-sensing
reflectance spectrum, given at the SeawiFS bands, a.k.a. [412 443 490 510 555 670] nm
This band setting is the same as of the ESA-OC-CCI Rrs, thus being
applicable to its very long time series for climate studies. Also, some
commercial in-situ radiometers use this bands and the algorithm can be
used for output data from them too.

**Relevant citations for algorithms**

  · Hue angle:  (vd Woerd and Wernand,  2015); (Pitarch et al. 2019)
  · Forel-Ule index:  (Novoa et al., 2013); (vd Woerd and Wernand,  2015); (Pitarch et al. 2019)
  · Secchi disk depth:  (Lee et al. 2002); (Lee et al. 2013); (Lee et al.,  2015)

**References**

Lee, Z., Shang, S., Hu, C., Du, K., Weidemann, A., Hou, W., ... & Lin, G. (2015). Secchi disk depth:
A new theory and mechanistic model for underwater visibility. Remote sensing of environment, 169, 139-149.

Lee, Z., Hu, C., Shang, S., Du, K., Lewis, M., Arnone, R., & Brewin, R. (2013). Penetration of
UV‐visible solar radiation in the global oceans: Insights from ocean color remote sensing.
Journal of Geophysical Research: Oceans, 118(9), 4241-4255.

Mason, J. D., Cone, M. T., & Fry, E. S. (2016). Ultraviolet (250–550 nm)
absorption spectrum of pure water. Applied optics, 55(25), 7163-7172.

Novoa, S., Wernand, M. R., & Van der Woerd, H. J. (2013). The Forel-Ule scale revisited spectrally:
preparation protocol, transmission measurements and chromaticity. Journal of the European
Optical Society-Rapid publications, 8.

Pitarch, J., van der Woerd, H. J., Brewin, R. J., & Zielinski, O. (2019). Optical properties
of Forel-Ule water types deduced from 15 years of global satellite ocean color observations.
Remote Sensing of Environment, 231, 111249.

Pope, R. M., & Fry, E. S. (1997). Absorption spectrum (380–700 nm) of pure water.
II. Integrating cavity measurements. Applied optics, 36(33), 8710-8723.

Woerd, H. J., & Wernand, M. R. (2015). True colour classification of natural waters with
medium-spectral resolution satellites: SeaWiFS, MODIS, MERIS and OLCI. Sensors, 15(10), 25663-25680.

Zhang, X., & Hu, L. (2009). Estimating scattering of pure water from density fluctuation
of the refractive index. Optics Express, 17(3), 1671-1678.

Please check on gitlab for the most recent version of this package:
https://gitlab.com/jaipipor/hue-fu-secchi

__author__ = "Jaime Pitarch"
__copyright__ = "Copyright 2021, Jaime Pitarch"
__license__ = "LGPL"
__version__ = "1.0"
__maintainer__ = "Jaime Pitarch"
__email__ = "jaipipor@msn.com"
__status__ = "Development"


**Dependencies**

None

**Usage**

-- External call --

The **MATLAB** version is called using the argument of a remote-sensing reflectance, expressed as a 6x1 or 1x6 vector. An example with output is given:

Rrs=[0.00654 0.00663 0.00624 0.00464 0.00263 0.00027];
[alfa,FU,z_SD]=Hue_FU_SD_from_Rrs_sw(Rrs)

alfa =
     2.162e+02

FU =
     3

z_SD =
  19.125

See the help of the file "Hue_FU_SD_from_Rrs_sw.m" for explanations.

The **IDL** version is called using the argument of a remote-sensing reflectance, expressed as a 6x1 or 1x6 vector. An example with output is given:

Rrs=[0.00654,0.00663,0.00624,0.00464,0.00263,0.00027]
Hue_FU_SD_from_Rrs_sw,Rrs,angle,FU,z_SD

See the help of the file "Hue_FU_SD_from_Rrs_sw.pro" for explanations.
